PROJECT_NAME ?= factorio_calc
VENV_ROOT ?= ${HOME}/.local/share/venv
VENV_DIR ?= ${VENV_ROOT}/${PROJECT_NAME}

ACTIVATE_PATH := ${VENV_DIR}/bin/activate

run:
	source "${ACTIVATE_PATH}" && python -m factorio_calc
.PHONY: run

fmt format:
	source "${ACTIVATE_PATH}" && python -m black factorio_calc
.PHONY: fmt format

setup: requirements.txt | ${ACTIVATE_PATH}
	source "${ACTIVATE_PATH}" && python -m pip install -r requirements.txt
.PHONY: setup

${ACTIVATE_PATH}:
	mkdir -p "${VENV_ROOT}"
	python -m venv "${VENV_DIR}"
