Edge cases to handle:

- Byproducts that can be exported
  - Handling whatever amount of byproduct is produced without influencing the ratio of outputs that we care about
  - Example: sand byproduct that we want to turn into landfill
- Byproducts that feed back into the graph
  - Downstream
  - Same node
  - Upstream
  - Can it handle kovarex and/or vulcanite?
- Byproducts that feed back into the graph w/ more than you put in
  - This is particularly troublesome because some initial input is needed to bootstrap but then it is no longer needed
  - Example: hydrogen chloride
- Items that are produced from nothing:
  - Example: oxygen from atmospheric condenser
