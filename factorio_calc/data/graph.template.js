// create a network
var container = document.getElementById("mynetwork");
var data = {
  nodes: new vis.DataSet([$NODES]),
  edges: new vis.DataSet([$EDGES]),
};
var options = {
  layout: {
    hierarchical: {
      enabled: true,
      sortMethod: "directed",
      direction: "LR",
      shakeTowards: "roots",
    },
  },
  interaction: {
    tooltipDelay: 0,
  },
  physics: {
    enabled: false,
    stabilization: true,
    hierarchicalRepulsion: {
      springConstant: 0.01,
    },
  },
  nodes: {
    color: {
      highlight: {
	border: "#ff0000",
      },
    },
    labelHighlightBold: false,
  },
  edges: {
    color: {
      color: "#2b7ce9",
      highlight: "#ff0000",
    },
    smooth: {
      enabled: true,
      type: "dynamic",
    },
    arrows: {
      to: {
	enabled: true,
	type: "array",
      },
      from: {
	enabled: false,
	type: "circle",
      },
    },
  },
};
var network = new vis.Network(container, data, options);
network.stabilize(1000);
