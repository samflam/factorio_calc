import math
from pathlib import Path
import tempfile
import webbrowser

from .calc import Graph


def show_network(graph: Graph):
    with open(Path(__file__).parent / "data" / "graph.template.html") as f:
        html_template = f.read()

    with open(Path(__file__).parent / "data" / "graph.template.js") as f:
        graph_template = f.read()

    with open(
        Path(__file__).parent / "data" / "vendor" / "vis-network.min.js",
        encoding="utf8",
    ) as f:
        vis_src = f.read()

    node_lines = []
    for id, node in graph.nodes.items():
        label = node.item.name
        if node.machine:
            machine_count = int(math.ceil(node.machine_count))
            title = (
                f"{node.rate} {node.item.name}\\n{machine_count} {node.machine.name}"
            )
        else:
            title = f"{node.rate} {node.item.name}\\nExternal input"
        node_lines.append(f'{{ id: "{id}", label: "{label}", title: "{title}" }}')
    edge_lines = []
    for (from_id, to_id), edge in graph.edges.items():
        width = max(2, min(20, edge.rate**0.5 / 2))
        title = f"{edge.rate} {edge.from_item.name}"
        edge_lines.append(
            f'{{ from: "{from_id}", to: "{to_id}", title: "{title}", width: {width} }}'
        )

    nodes = ",".join(node_lines)
    edges = ",".join(edge_lines)

    graph_src = graph_template.replace("$NODES", nodes).replace("$EDGES", edges)
    html_text = html_template.format(VIS_SRC=vis_src, GRAPH_SRC=graph_src)
    with tempfile.NamedTemporaryFile(
        delete=False, suffix=".html", mode="w", encoding="utf-8"
    ) as f:
        f.write(html_text)
    webbrowser.open(f.name)
