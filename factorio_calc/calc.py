from .datatypes import Item, Recipe, Machine

from dataclasses import dataclass


class Graph:
    def __init__(self):
        self.nodes = {}
        self.edges = {}

    def clone(self):
        result = Graph()
        result.nodes = {k: v.clone() for k, v in self.nodes.items()}
        result.edges = {k: v.clone() for k, v in self.edges.items()}
        return result

    def scale(self, item: Item, rate: float) -> "Graph":
        for k, node in self.nodes.items():
            if node.item == item:
                ratio = rate / node.rate
                break
        else:
            raise KeyError

        result = self.clone()
        for node in result.nodes.values():
            node.rate *= ratio
            node.machine_count *= ratio
        for edge in result.edges.values():
            edge.rate *= ratio
        return result


@dataclass
class Node:
    item: Item
    recipe: Recipe | None
    machine: Machine | None
    rate: float = 0.0
    machine_count: float = 0.0

    def clone(self):
        return Node(
            item=self.item,
            recipe=self.recipe,
            machine=self.machine,
            rate=self.rate,
            machine_count=self.machine_count,
        )


@dataclass
class Edge:
    from_item: Item
    to_item: Item
    rate: float = 0.0

    def clone(self):
        return Edge(
            from_item=self.from_item,
            to_item=self.to_item,
            rate=self.rate,
        )


# productivity impact from a single prod module
productivity_impact = [0, 0.04, 0.06, 0.08]

# speed impact from a single prod module
productivity_speed_impact = [0, -0.1, -0.15, -0.20]

# speed impact from a single speed module
speed_impact = [0, 0.2, 0.3, 0.4]


def compute_graph(
    db,
    input_items: list[str],
    output_items: dict[str, float] | list[str],
    productivity_level=3,
    speed_level=3,
    beacon_modules=4,
) -> Graph:
    input_items = [db.find_item(x) for x in input_items]
    if isinstance(output_items, list):
        output_items = [(x, 1) for x in output_items]
    output_items = [(db.find_item(x1), x2) for x1, x2 in output_items]

    g = Graph()
    pending_items = []

    for item, ratio in output_items:
        pending_items.append((item, ratio))
    while pending_items:
        item, ratio = pending_items.pop()
        if item.id not in g.nodes:
            if item in input_items:
                recipe = None
                machine = None
            else:
                recipes = db.find_item_recipes(item)
                if len(recipes) != 1:
                    raise ValueError(f"Not exactly one recipe for item {item.name}")
                recipe = recipes[0]
                machines = db.find_recipe_machine(recipe)
                assert len(machines) == 1
                machine = machines[0]
            g.nodes[item.id] = Node(item=item, recipe=recipe, machine=machine)
        node = g.nodes[item.id]
        node.rate += ratio

        if node.recipe is None:
            continue

        inputs = db.find_recipe_inputs(node.recipe)
        outputs = db.find_recipe_outputs(node.recipe)
        # ignoring byproducts
        product_output = [x for x in outputs if x.item_id == item.id][0]
        product_ev = (product_output.low + product_output.high) / 2
        allow_productivity = (
            node.recipe.allow_productivity and node.machine.allow_productivity
        )
        if allow_productivity:
            prod_modules = node.machine.module_slots
            speed_modules = beacon_modules
        else:
            prod_modules = 0
            speed_modules = node.machine.module_slots + beacon_modules
        productivity_ratio = 1 + (
            productivity_impact[productivity_level] * prod_modules
        )
        recipe_ratio = ratio / (product_ev * productivity_ratio)
        module_speed = 1 + (
            productivity_speed_impact[productivity_level] * prod_modules
            + speed_impact[speed_level] * speed_modules
        )
        machine_count = (
            recipe_ratio * node.recipe.duration / node.machine.speed / module_speed
        )

        node.machine_count += machine_count
        for input_ in inputs:
            edge_key = (input_.item_id, item.id)
            if edge_key not in g.edges:
                input_item = db.get_item(input_.item_id)
                g.edges[edge_key] = Edge(to_item=item, from_item=input_item)
            edge = g.edges[edge_key]
            input_ratio = recipe_ratio * input_.count
            edge.rate += input_ratio
            input_item = db.get_item(input_.item_id)
            pending_items.append((input_item, input_ratio))
    return g
