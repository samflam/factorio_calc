import uuid
import re

from .datatypes import Item, Recipe, Machine, RecipeInput, RecipeOutput, RecipeMachine


def gen_id() -> str:
    return str(uuid.uuid4())


class Database:
    def __init__(self):
        self.items = []
        self.recipes = []
        self.machines = []
        self.recipe_inputs = []
        self.recipe_outputs = []
        self.recipe_machines = []

    def add_item(self, *, name) -> Item:
        item = Item(id=name, name=name)
        assert not any(x for x in self.items if x.id == item.id)
        self.items.append(item)
        return item

    def add_machine(
        self, *, name: str, speed: float, module_slots: int, allow_productivity: bool
    ) -> Machine:
        machine = Machine(
            id=gen_id(),
            name=name,
            speed=speed,
            module_slots=module_slots,
            allow_productivity=allow_productivity,
        )
        assert not any(x for x in self.machines if x.id == machine.id)
        self.machines.append(machine)
        return machine

    def add_recipe(
        self, *, name: str = "unnamed", duration: float, allow_productivity: bool
    ) -> Recipe:
        recipe = Recipe(
            id=gen_id(),
            name=name,
            duration=duration,
            allow_productivity=allow_productivity,
        )
        assert not any(x for x in self.recipes if x.id == recipe.id)
        self.recipes.append(recipe)
        return recipe

    def add_recipe_input(self, recipe: Recipe, item: Item, count: int) -> RecipeInput:
        assert any(x for x in self.recipes if x.id == recipe.id)
        assert any(x for x in self.items if x.id == item.id)
        assert not any(
            x
            for x in self.recipe_inputs
            if x.recipe_id == recipe.id and x.item_id == item.id
        )
        input_ = RecipeInput(recipe_id=recipe.id, item_id=item.id, count=count)
        self.recipe_inputs.append(input_)
        return input_

    def add_recipe_output(
        self, recipe: Recipe, item: Item, high: float, low: float | None = None
    ) -> RecipeOutput:
        if low is None:
            low = high
        assert any(x for x in self.recipes if x.id == recipe.id)
        assert any(x for x in self.items if x.id == item.id)
        assert not any(
            x
            for x in self.recipe_outputs
            if x.recipe_id == recipe.id and x.item_id == item.id
        )
        output = RecipeOutput(recipe_id=recipe.id, item_id=item.id, low=low, high=high)
        self.recipe_outputs.append(output)
        return output

    def add_recipe_machine(self, *, recipe: Recipe, machine: Machine) -> RecipeMachine:
        assert any(x for x in self.recipes if x.id == recipe.id)
        assert any(x for x in self.machines if x.id == machine.id)
        assert not any(
            x
            for x in self.recipe_machines
            if x.recipe_id == recipe.id and x.machine_id == machine.id
        )
        recipe_machine = RecipeMachine(recipe_id=recipe.id, machine_id=machine.id)
        self.recipe_machines.append(recipe_machine)
        return recipe_machine

    def get_item(self, id: str):
        for x in self.items:
            if x.id == id:
                return x
        raise KeyError

    def get_recipe(self, id: str):
        for x in self.recipes:
            if x.id == id:
                return x
        raise KeyError

    def get_machine(self, id: str):
        for x in self.machines:
            if x.id == id:
                return x
        raise KeyError

    def find_item_recipes(self, item: Item) -> list[Recipe]:
        return [
            self.get_recipe(x.recipe_id)
            for x in self.recipe_outputs
            if x.item_id == item.id
        ]

    def find_recipe_inputs(self, recipe: Recipe) -> list[RecipeInput]:
        return [x for x in self.recipe_inputs if x.recipe_id == recipe.id]

    def find_recipe_outputs(self, recipe: Recipe) -> list[RecipeOutput]:
        return [x for x in self.recipe_outputs if x.recipe_id == recipe.id]

    def find_item(self, search: str | Item):
        if isinstance(search, Item):
            return search

        def simplify(name: str) -> str:
            return re.sub("[-_ ]", "", name.lower().strip())

        search = simplify(search)
        items = {simplify(x.name): x for x in self.items}
        if search in items:
            return items[search]
        matches = []
        for name, item in items.items():
            if search in name:
                matches.append(name)
        if not matches:
            raise KeyError(str(search))
        if len(matches) > 1:
            matches = [items[nm].name for nm in matches]
            raise ValueError(f"Ambiguous item search: {matches=}")
        return items[matches[0]]

    def find_recipe_machine(self, recipe: Recipe) -> list[Machine]:
        return [
            self.get_machine(x.machine_id)
            for x in self.recipe_machines
            if x.recipe_id == recipe.id
        ]
