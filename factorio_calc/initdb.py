from .database import Database
from .datatypes import Item


def init_db():
    db = Database()

    assembler3 = db.add_machine(
        name="Assembling machine 3", speed=1.25, module_slots=4, allow_productivity=True
    )

    atmos_condensor = db.add_machine(
        name="Atmospheric condenser",
        speed=1.5,
        module_slots=2,
        allow_productivity=False,
    )

    fuel_refinery = db.add_machine(
        name="Fuel refinery", speed=1, module_slots=3, allow_productivity=True
    )

    pulverizer = db.add_machine(
        name="Pulverizer", speed=2, module_slots=4, allow_productivity=True
    )

    ind_furnace = db.add_machine(
        name="Industrial furnace", speed=4, module_slots=5, allow_productivity=True
    )

    iron_plate = db.add_item(name="Iron plate")
    steel_plate = db.add_item(name="Steel plate")
    copper_plate = db.add_item(name="Copper plate")
    advanced_circuit = db.add_item(name="Advanced circuit")
    light_oil = db.add_item(name="Light oil")
    stone = db.add_item(name="Stone")
    stone_brick = db.add_item(name="Stone brick")
    sulfur = db.add_item(name="Sulfur")
    plastic_bar = db.add_item(name="Plastic bar")
    battery = db.add_item(name="Battery")
    beryllium_plate = db.add_item(name="Beryllium plate")

    def add_stone_tablet() -> Item:
        stone_tablet = db.add_item(name="Stone tablet")
        recipe = db.add_recipe(duration=0.5, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=stone_brick, count=1)
        db.add_recipe_output(recipe=recipe, item=stone_tablet, high=4)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return stone_tablet

    stone_tablet = add_stone_tablet()

    def add_copper_cable() -> Item:
        copper_cable = db.add_item(name="Copper cable")
        recipe = db.add_recipe(duration=0.5, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=copper_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=copper_cable, high=2)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return copper_cable

    copper_cable = add_copper_cable()

    def add_electronic_circuit() -> Item:
        electronic_circuit = db.add_item(name="Electronic circuit")
        recipe = db.add_recipe(duration=0.5, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=copper_cable, count=3)
        db.add_recipe_input(recipe=recipe, item=stone_tablet, count=1)
        db.add_recipe_output(recipe=recipe, item=electronic_circuit, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return electronic_circuit

    electronic_circuit = add_electronic_circuit()

    def add_iron_gear() -> Item:
        iron_gear = db.add_item(name="Iron gear wheel")
        recipe = db.add_recipe(duration=0.5, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=iron_gear, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return iron_gear

    iron_gear = add_iron_gear()

    def add_iron_stick() -> Item:
        iron_stick = db.add_item(name="Iron stick")
        recipe = db.add_recipe(duration=0.5, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=iron_stick, high=2)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return iron_stick

    iron_stick = add_iron_stick()

    def add_iron_beam() -> Item:
        iron_beam = db.add_item(name="Iron beam")
        recipe = db.add_recipe(duration=2, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=2)
        db.add_recipe_output(recipe=recipe, item=iron_beam, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return iron_beam

    iron_beam = add_iron_beam()

    def add_steel_beam() -> Item:
        steel_beam = db.add_item(name="Steel beam")
        recipe = db.add_recipe(duration=2, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=steel_plate, count=2)
        db.add_recipe_output(recipe=recipe, item=steel_beam, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return steel_beam

    steel_beam = add_steel_beam()

    def add_pipe() -> Item:
        pipe = db.add_item(name="Pipe")
        recipe = db.add_recipe(duration=0.5, allow_productivity=False)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=pipe, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return pipe

    pipe = add_pipe()

    def add_small_motor() -> Item:
        small_motor = db.add_item(name="Small electric motor")
        recipe = db.add_recipe(duration=0.8, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_gear, count=1)
        db.add_recipe_input(recipe=recipe, item=copper_cable, count=6)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=small_motor, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return small_motor

    small_motor = add_small_motor()

    def add_iron_chest() -> Item:
        iron_chest = db.add_item(name="Iron chest")
        recipe = db.add_recipe(duration=0.5, allow_productivity=False)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=8)
        db.add_recipe_output(recipe=recipe, item=iron_chest, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return iron_chest

    iron_chest = add_iron_chest()

    def add_inserter_parts() -> Item:
        inserter_parts = db.add_item(name="Inserter parts")
        recipe = db.add_recipe(duration=2, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_stick, count=4)
        db.add_recipe_input(recipe=recipe, item=iron_gear, count=4)
        db.add_recipe_output(recipe=recipe, item=inserter_parts, high=2)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return inserter_parts

    inserter_parts = add_inserter_parts()

    def add_engine() -> Item:
        engine = db.add_item(name="Single-cylinder engine")
        recipe = db.add_recipe(duration=0.6, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_gear, count=1)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=engine, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return engine

    engine = add_engine()

    def add_burner_inserter() -> Item:
        burner_inserter = db.add_item(name="Burner inserter")
        recipe = db.add_recipe(duration=0.5, allow_productivity=False)
        db.add_recipe_input(recipe=recipe, item=engine, count=1)
        db.add_recipe_input(recipe=recipe, item=inserter_parts, count=1)
        db.add_recipe_output(recipe=recipe, item=burner_inserter, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return burner_inserter

    burner_inserter = add_burner_inserter()

    def add_automation_core() -> Item:
        automation_core = db.add_item(name="Automation core")
        recipe = db.add_recipe(duration=2, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_stick, count=4)
        db.add_recipe_input(recipe=recipe, item=iron_gear, count=4)
        db.add_recipe_input(recipe=recipe, item=copper_plate, count=6)
        db.add_recipe_output(recipe=recipe, item=automation_core, high=2)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return automation_core

    automation_core = add_automation_core()

    def add_inserter() -> Item:
        inserter = db.add_item(name="Inserter")
        recipe = db.add_recipe(duration=0.5, allow_productivity=False)
        db.add_recipe_input(recipe=recipe, item=small_motor, count=1)
        db.add_recipe_input(recipe=recipe, item=automation_core, count=1)
        db.add_recipe_input(recipe=recipe, item=burner_inserter, count=1)
        db.add_recipe_output(recipe=recipe, item=inserter, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return inserter

    inserter = add_inserter()

    def add_fast_inserter() -> Item:
        fast_inserter = db.add_item(name="Fast inserter")
        recipe = db.add_recipe(duration=0.5, allow_productivity=False)
        db.add_recipe_input(recipe=recipe, item=electronic_circuit, count=2)
        db.add_recipe_input(recipe=recipe, item=inserter, count=1)
        db.add_recipe_input(recipe=recipe, item=steel_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=fast_inserter, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return fast_inserter

    fast_inserter = add_fast_inserter()

    def add_storage_tank() -> Item:
        storage_tank = db.add_item(name="Storage tank")
        recipe = db.add_recipe(duration=3, allow_productivity=False)
        db.add_recipe_input(recipe=recipe, item=iron_beam, count=5)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=10)
        db.add_recipe_output(recipe=recipe, item=storage_tank, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return storage_tank

    storage_tank = add_storage_tank()

    def add_pump() -> Item:
        pump = db.add_item(name="Pump")
        recipe = db.add_recipe(duration=2, allow_productivity=False)
        db.add_recipe_input(recipe=recipe, item=small_motor, count=2)
        db.add_recipe_input(recipe=recipe, item=pipe, count=2)
        db.add_recipe_input(recipe=recipe, item=steel_plate, count=1)
        db.add_recipe_output(recipe=recipe, item=pump, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return pump

    pump = add_pump()

    def add_rocket_fuel_tank() -> Item:
        fuel_tank = db.add_item(name="Rocket fuel tank")
        recipe = db.add_recipe(duration=30, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=advanced_circuit, count=2)
        db.add_recipe_input(recipe=recipe, item=pipe, count=2)
        db.add_recipe_input(recipe=recipe, item=storage_tank, count=1)
        db.add_recipe_input(recipe=recipe, item=pump, count=1)
        db.add_recipe_output(recipe=recipe, item=fuel_tank, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return fuel_tank

    rocket_fuel_tank = add_rocket_fuel_tank()

    def add_cargo_pod() -> Item:
        cargo_pod = db.add_item(name="Cargo pod")
        recipe = db.add_recipe(duration=30, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=advanced_circuit, count=4)
        db.add_recipe_input(recipe=recipe, item=iron_chest, count=4)
        db.add_recipe_input(recipe=recipe, item=fast_inserter, count=2)
        db.add_recipe_input(recipe=recipe, item=steel_plate, count=6)
        db.add_recipe_output(recipe=recipe, item=cargo_pod, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return cargo_pod

    cargo_pod = add_cargo_pod()

    def add_oxygen() -> Item:
        oxygen = db.add_item(name="Oxygen")
        recipe = db.add_recipe(duration=10, allow_productivity=False)
        db.add_recipe_output(recipe=recipe, item=oxygen, high=300)
        db.add_recipe_machine(recipe=recipe, machine=atmos_condensor)
        return oxygen

    oxygen = add_oxygen()

    def add_solid_rocket_fuel() -> Item:
        solid_rocket_fuel = db.add_item(name="Solid rocket fuel")
        recipe = db.add_recipe(duration=1, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=1)
        db.add_recipe_input(recipe=recipe, item=light_oil, count=100)
        db.add_recipe_input(recipe=recipe, item=oxygen, count=1000)
        db.add_recipe_output(recipe=recipe, item=solid_rocket_fuel, high=1)
        db.add_recipe_machine(recipe=recipe, machine=fuel_refinery)
        return solid_rocket_fuel

    solid_rocket_fuel = add_solid_rocket_fuel()

    def add_sand() -> Item:
        sand = db.add_item(name="Sand")
        recipe = db.add_recipe(duration=1, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=stone, count=3)
        db.add_recipe_output(recipe=recipe, item=sand, low=7, high=8)
        db.add_recipe_machine(recipe=recipe, machine=pulverizer)
        return sand

    sand = add_sand()

    def add_glass() -> Item:
        glass = db.add_item(name="Glass")
        recipe = db.add_recipe(duration=16, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=sand, count=16)
        db.add_recipe_output(recipe=recipe, item=glass, high=8)
        db.add_recipe_machine(recipe=recipe, machine=ind_furnace)
        return glass

    glass = add_glass()

    def add_heat_shielding() -> Item:
        heat_shielding = db.add_item(name="Heat shielding")
        recipe = db.add_recipe(duration=10, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=sulfur, count=8)
        db.add_recipe_input(recipe=recipe, item=stone_tablet, count=20)
        db.add_recipe_input(recipe=recipe, item=steel_plate, count=2)
        db.add_recipe_output(recipe=recipe, item=heat_shielding, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return heat_shielding

    heat_shielding = add_heat_shielding()

    def add_low_density_structure() -> Item:
        lds = db.add_item(name="Low density structure")
        recipe = db.add_recipe(duration=10, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=plastic_bar, count=10)
        db.add_recipe_input(recipe=recipe, item=copper_plate, count=10)
        db.add_recipe_input(recipe=recipe, item=steel_plate, count=2)
        db.add_recipe_output(recipe=recipe, item=lds, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return lds

    low_density_structure = add_low_density_structure()

    def add_rocket_control_unit() -> Item:
        rcu = db.add_item(name="Rocket control unit")
        recipe = db.add_recipe(duration=30, allow_productivity=True)
        db.add_recipe_input(recipe=recipe, item=battery, count=5)
        db.add_recipe_input(recipe=recipe, item=advanced_circuit, count=5)
        db.add_recipe_input(recipe=recipe, item=glass, count=5)
        db.add_recipe_input(recipe=recipe, item=iron_plate, count=5)
        db.add_recipe_output(recipe=recipe, item=rcu, high=1)
        db.add_recipe_machine(recipe=recipe, machine=assembler3)
        return rcu

    rocket_control_unit = add_rocket_control_unit()

    def add_cargo_rocket_section() -> Item:
        cargo_section = db.add_item(name="Cargo rocket section")

        # No beryllium recipe
        # recipe_basic = db.add_recipe(duration=30, allow_productivity=True)
        # db.add_recipe_input(recipe=recipe_basic, item=low_density_structure, count=4)
        # db.add_recipe_input(recipe=recipe_basic, item=rocket_control_unit, count=4)
        # db.add_recipe_input(recipe=recipe_basic, item=cargo_pod, count=1)
        # db.add_recipe_input(recipe=recipe_basic, item=rocket_fuel_tank, count=1)
        # db.add_recipe_input(recipe=recipe_basic, item=heat_shielding, count=4)
        # db.add_recipe_output(recipe=recipe_basic, item=cargo_section, high=1)
        # db.add_recipe_machine(recipe=recipe_basic, machine=assembler3)

        # Yes beryllium recipe
        recipe_good = db.add_recipe(duration=60, allow_productivity=True)
        db.add_recipe_input(recipe=recipe_good, item=low_density_structure, count=4)
        db.add_recipe_input(recipe=recipe_good, item=rocket_control_unit, count=4)
        db.add_recipe_input(recipe=recipe_good, item=cargo_pod, count=1)
        db.add_recipe_input(recipe=recipe_good, item=rocket_fuel_tank, count=1)
        db.add_recipe_input(recipe=recipe_good, item=heat_shielding, count=4)
        db.add_recipe_input(recipe=recipe_good, item=beryllium_plate, count=8)
        db.add_recipe_output(recipe=recipe_good, item=cargo_section, high=2)
        db.add_recipe_machine(recipe=recipe_good, machine=assembler3)

        return cargo_section

    cargo_rocket_section = add_cargo_rocket_section()

    return db
