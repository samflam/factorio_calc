from dataclasses import dataclass


@dataclass
class Recipe:
    id: str
    duration: float
    allow_productivity: bool
    name: str = "unnamed"


@dataclass
class Item:
    id: str
    name: str = "unnamed"


@dataclass
class Machine:
    id: str
    speed: float
    module_slots: int
    allow_productivity: bool
    name: str = "unnamed"


@dataclass
class RecipeInput:
    item_id: str
    recipe_id: str
    count: int


@dataclass
class RecipeOutput:
    item_id: str
    recipe_id: str
    low: float
    high: float


@dataclass
class RecipeMachine:
    recipe_id: str
    machine_id: str
