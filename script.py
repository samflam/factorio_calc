from factorio_calc.initdb import init_db
from factorio_calc.calc import compute_graph
from factorio_calc.view import show_network

db = init_db()
graph = compute_graph(
    db,
    input_items=[
        "beryllium plate",
        "advanced circuit",
        "battery",
        "steel plate",
        "glass",
        "plastic bar",
        "iron plate",
        "copper plate",
        "cargo pod",
        "rocket fuel tank",
        "heat shielding",
    ],
    output_items=["cargo rocket section"],
    productivity_level=3,
)
graph = graph.scale(db.find_item("cargo rocket section"), 15)
show_network(graph)
