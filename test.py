from factorio_calc.initdb import init_db
from factorio_calc.calc import compute_graph
from factorio_calc.view import show_network

db = init_db()
graph = compute_graph(db, input_items=["copper plate"], output_items=["copper cable"])
graph = graph.scale(db.find_item("copper plate"), 1000)
show_network(graph)
